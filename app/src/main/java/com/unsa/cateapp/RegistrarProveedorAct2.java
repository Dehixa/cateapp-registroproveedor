package com.unsa.cateapp;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistrarProveedorAct2 extends AppCompatActivity {

    // Campos editables
    TextInputEditText direccion;
    TextInputEditText distrito;
    TextInputEditText provincia;
    TextInputEditText departamento;

    // Layouts de los campos editables
    TextInputLayout layout_direccion;
    TextInputLayout layout_distrito;
    TextInputLayout layout_provincia;
    TextInputLayout layout_departamento;

    String datos1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_proveedor_act2);

        direccion = findViewById(R.id.editTextDireccion);
        distrito = findViewById(R.id.editTextDistrito);
        provincia = findViewById(R.id.editTextProvincia);
        departamento = findViewById(R.id.editTextDepartamento);

        layout_direccion = findViewById(R.id.direccion_text_input);
        layout_distrito = findViewById(R.id.distrito_text_input);
        layout_provincia = findViewById(R.id.provincia_text_input);
        layout_departamento = findViewById(R.id.departamento_text_input);

        Button btn_sgte2 = findViewById(R.id.btn_Siguiente2);

        datos1 = getIntent().getExtras().getString("datos1");

        btn_sgte2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validarDatos()){
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("direccion", direccion.getText().toString());
                        jsonObject.put("distrito", distrito.getText().toString());
                        jsonObject.put("provincia", provincia.getText().toString());
                        jsonObject.put("departamento", departamento.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(RegistrarProveedorAct2.this, RegistrarProveedorAct3.class);
                    intent.putExtra("datos1", datos1);
                    intent.putExtra("datos2", jsonObject.toString());
                    startActivity(intent);
                }

            }
        });
    }

    // Validacion de campos de resgistro
    private boolean validarDatos() {
        boolean valido = true;

        // Comprueba que el campo de direccion no este vacio.
        if (TextUtils.isEmpty(direccion.getText().toString())) {
            layout_direccion.setError("Debe ingresar una Dirección.");
            layout_direccion.setErrorEnabled(true);
            valido = false;
        } else {
            layout_direccion.setErrorEnabled(false);
        }

        // Comprueba que el campo de distrito no este vacio.
        if (TextUtils.isEmpty(distrito.getText().toString())) {
            layout_distrito.setError("Debe ingresar un Distrito.");
            layout_distrito.setErrorEnabled(true);
            valido = false;
        } else {
            layout_distrito.setErrorEnabled(false);
        }

        // Comprueba que el campo de provincia no este vacio.
        if (TextUtils.isEmpty(provincia.getText().toString())) {
            layout_provincia.setError("Debe ingresar una Provincia.");
            layout_provincia.setErrorEnabled(true);
            valido = false;
        } else {
            layout_provincia.setErrorEnabled(false);
        }

        // Comprueba que el campo de departamento no este vacio.
        if (TextUtils.isEmpty(departamento.getText().toString())) {
            layout_departamento.setError("Debe ingresar un Departamento.");
            layout_departamento.setErrorEnabled(true);
            valido = false;
        } else {
            layout_departamento.setErrorEnabled(false);
        }

        return valido;
    }
}