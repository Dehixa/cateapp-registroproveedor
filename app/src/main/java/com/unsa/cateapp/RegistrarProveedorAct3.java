package com.unsa.cateapp;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.unsa.cateapp.ConexionRed.AsyncTaskListener;
import com.unsa.cateapp.ConexionRed.HostURL;
import com.unsa.cateapp.ConexionRed.HttpTask;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistrarProveedorAct3 extends AppCompatActivity {

    // Campos editables
    TextInputEditText nombres;
    TextInputEditText apePaterno;
    TextInputEditText apeMaterno;
    TextInputEditText password;

    // Layouts de los campos editables
    TextInputLayout layout_nombres;
    TextInputLayout layout_apePat;
    TextInputLayout layout_apeMat;
    TextInputLayout layout_password;

    String datos1;
    String datos2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_proveedor_act3);

        nombres = findViewById(R.id.editTextNombresPropietario);
        apePaterno = findViewById(R.id.editTextApePatPropietario);
        apeMaterno = findViewById(R.id.editTextApeMatPropietario);
        password = findViewById(R.id.editTextPassword);

        layout_nombres = findViewById(R.id.nombres_propietario_text_input);
        layout_apePat = findViewById(R.id.apePat_propietario_text_input);
        layout_apeMat = findViewById(R.id.apeMat_propietario_text_input);
        layout_password = findViewById(R.id.password_text_input);

        Button btn_registrar = findViewById(R.id.btn_Registrar_Proveedor);

        datos1 = getIntent().getExtras().getString("datos1");
        datos2 = getIntent().getExtras().getString("datos2");

        btn_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validarDatos()){
                    JSONObject jsonObject = new JSONObject();
                    try {
                        JSONObject jsonDatos1 = new JSONObject(datos1);
                        JSONObject jsonDatos2 = new JSONObject(datos2);

                        jsonObject.put("metodo", 3);
                        jsonObject.put("razonSocial", jsonDatos1.getString("razonSocial"));
                        jsonObject.put("ruc", jsonDatos1.getString("ruc"));
                        jsonObject.put("email", jsonDatos1.getString("email"));
                        jsonObject.put("paginaWeb", jsonDatos1.getString("paginaWeb"));
                        jsonObject.put("telefono", jsonDatos1.getString("telefono"));

                        jsonObject.put("direccion", jsonDatos2.getString("direccion"));
                        jsonObject.put("distrito", jsonDatos2.getString("distrito"));
                        jsonObject.put("provincia", jsonDatos2.getString("provincia"));
                        jsonObject.put("departamento", jsonDatos2.getString("departamento"));

                        jsonObject.put("nombres", nombres.getText().toString());
                        jsonObject.put("apePat", apePaterno.getText().toString());
                        jsonObject.put("apeMat", apeMaterno.getText().toString());
                        jsonObject.put("password", password.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("****************************************+");
                    //System.out.println(jsonObject.toString());
                    Log.d("tag", jsonObject.toString());
                    System.out.println("+***************************************+");

                    new HttpTask(new AsyncTaskListener() {
                        @Override
                        public void onHttpTask(String data) {
                            System.out.println("******************************************");
                            System.out.println(data);

                            try {
                                JSONObject respuesta = new JSONObject(data);

                                Toast.makeText(RegistrarProveedorAct3.this, respuesta.getString("message"), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }).execute(HostURL.REGISTRAR_USUARIO, jsonObject.toString());
                }

            }
        });
    }

    // Validacion de campos de resgistro
    private boolean validarDatos() {
        boolean valido = true;

        // Comprueba que el campo de Nombres no este vacio.
        if (TextUtils.isEmpty(nombres.getText().toString())) {
            layout_nombres.setError("Debe ingresar sus Nombres.");
            layout_nombres.setErrorEnabled(true);
            valido = false;
        } else {
            layout_nombres.setErrorEnabled(false);
        }

        // Comprueba que el campo de apellido paterno no este vacio.
        if (TextUtils.isEmpty(apePaterno.getText().toString())) {
            layout_apePat.setError("Debe ingresar su Apellido Paterno.");
            layout_apePat.setErrorEnabled(true);
            valido = false;
        } else {
            layout_apePat.setErrorEnabled(false);
        }

        // Comprueba que el campo de Nombres no este vacio.
        if (TextUtils.isEmpty(apeMaterno.getText().toString())) {
            layout_apeMat.setError("Debe ingresar su Apellido Materno.");
            layout_apeMat.setErrorEnabled(true);
            valido = false;
        } else {
            layout_apeMat.setErrorEnabled(false);
        }

        // Comprueba que el campo de password no este vacio.
        if (TextUtils.isEmpty(password.getText().toString())) {
            layout_password.setError("Debe ingresar una Contraseña.");
            layout_password.setErrorEnabled(true);
            valido = false;
        } else {
            layout_password.setErrorEnabled(false);
        }

        return valido;
    }
}