package com.unsa.cateapp.ConexionRed;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpTask extends AsyncTask<String, Void, String> {

    // variable de escucha para almacenar una implementación particular de las
    // devoluciones de llamada que definirá el propietario
    private AsyncTaskListener listener;

    public HttpTask(AsyncTaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... strings) {
        String result = "";

        try {
            String urlHost = strings[0];
            String json = strings[1];

            URL url = new URL(urlHost);
            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();

            // Tiempo de espera para connection.connect () establecido arbitrariamente a 5000ms.
            conexion.setConnectTimeout(10000);
            conexion.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conexion.setRequestMethod("POST");

            // En True indica que la aplicación tiene la intención de escribir datos en la conexión URL.
            conexion.setDoOutput(true);
            // En True indica que la aplicación tiene la intención de leer los datos de la conexión URL.
            conexion.setDoInput(true);

            // Enlace de comunicaciones abierto (el tráfico de red se produce aquí).
            conexion.connect();

            // Transmite los datos escribiendo en el flujo devuelto por URLConnection.getOutputStream ().
            OutputStream os = conexion.getOutputStream();
            os.write(json.getBytes());
            os.close();

            // El cuerpo de la respuesta se puede leer de la secuencia devuelta por URLConnection.getInputStream ().
            // Si la respuesta no tiene cuerpo, ese método devuelve una secuencia vacía.
            InputStream stream = new BufferedInputStream(conexion.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));

            StringBuilder builder = new StringBuilder();
            String inputString;
            while ((inputString = bufferedReader.readLine()) != null) {
                builder.append(inputString);
            }
            result = builder.toString();

            conexion.disconnect();
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        listener.onHttpTask(s);
    }
}
